#include <stdio.h> 
#include <stdlib.h> 
#include <errno.h> 
#include <math.h>
#include <time.h>
/****************************************************************************
* IMPORTANT NOTE FOR STUDENTS:
  ----------------------------
  USE THIS OPTION FOR COTAN_A OR COTAN_B VERSION:
  - Leave commented the following line to launch version B of the cotangent calculation
  - Uncomment the following line to launch version A of the cotangent calculation
*****************************************************************************/
//#define COTAN_A 

/****************************************************************************
* IMPORTANT NOTE FOR STUDENTS:
  ----------------------------
  USE THIS OPTION FOR A SINGLE-THREADED OR MULTI-THREADED VERSION:
  - Leave commented the following line to launch monohilo version of the program
  - Uncomment the following line to launch multi-threaded version of the program
*****************************************************************************/
#define MULTI_THREADED  

const unsigned long nElements = 10000;
const unsigned long nTimes = 5000;
const unsigned int MAX_THREADS = 10;

void Task(unsigned long nElements, unsigned long nTimes);
double CoTan_A(double a);
double CoTan_B(double a);

#ifdef MULTI_THREADED
#include <pthread.h>

	void * ThreadJob(void * dummy)
	{
		Task(nElements, nTimes);
		return NULL;
	}
#endif


int main(int argc, char* argv[])
{
	struct timespec tStart, tEnd;
	double dElapsedTimeS;
	
	// Start measuring time
	if (clock_gettime(CLOCK_REALTIME, &tStart));
	
	#ifdef MULTI_THREADED
	if (argc != 2)
	{
		fprintf(stderr, "Usage: %s <thread_count>\n", argv[0]);
		exit(EXIT_FAILURE);
	}
	unsigned int nThreads = atoi(argv[1]);
	if ((nThreads <= 0) || (nThreads > MAX_THREADS))
	{
		fprintf(stderr, "ERROR: thread count should be between [1,10]\n");
		exit(EXIT_FAILURE);
	}

	printf("Running with %d thread(s)...\n", nThreads);
	pthread_t Threads[nThreads];

	// Start threads
	for (int i = 0; i < nThreads; i++)
	{
		int pthread_ret = pthread_create(&Threads[i], NULL, ThreadJob, NULL);
		if (pthread_ret)
		{
			fprintf(stderr, "ERROR: pthread_create error code: %d.\n", pthread_ret);
			exit(EXIT_FAILURE);
		}
	}

	// Wait for the threads to finish
	for (int i = 0; i < nThreads; i++)
	{
		pthread_join(Threads[i], NULL);
	}
#else
	// The single thread of the process runs the task
	Task(nElements,nTimes);
#endif

	// Finish measuring time
	if (clock_gettime(CLOCK_REALTIME, &tEnd));

	printf("Execution finished\n");

	// Show the elapsed time
	dElapsedTimeS = (tEnd.tv_sec - tStart.tv_sec);
    	dElapsedTimeS += (tEnd.tv_nsec - tStart.tv_nsec) / 1e+9;
	printf("Elapsed time    : %f s.\n", dElapsedTimeS);
	
	return 0;
}

double getRandom(unsigned int min, unsigned int max)
{
	// Pseudo-random numbers in the interval [min,max]
	return min + (max - min) * ((double)rand()/(double)RAND_MAX);
}

void Task(unsigned long nElements, unsigned long nTimes)
{
	unsigned long i, j;
	static unsigned int seed = 0;

	double *pdSrc = (double*)malloc(nElements * sizeof(double));
	double *pdDest = (double*)malloc(nElements * sizeof(double));
	if (pdSrc == NULL || pdDest == NULL)
	{
		free(pdSrc);
		free(pdDest);
		printf("ERROR in Task: Cannot allocate memory\n");
		return;
	}

	srand(seed++);
	for (i = 0; i < nElements; i++)
	{
		// Pseudo-random numbers in the interval [1.0-2.0]
		pdSrc[i] = getRandom(1,2);
	}

	for (j = 0; j < nTimes; j++)
	{
		for (i = 0; i < nElements; i++)
		{
			#ifdef COTAN_A
				pdDest[i] = CoTan_A(pdSrc[i]);
			#else
				pdDest[i] = CoTan_B(pdSrc[i]);
			#endif
		}
	}

	free(pdSrc);
 	free(pdDest);
}

double CoTan_A(double a)
{
	return 1/tan(a); 
}

double CoTan_B(double a)
{
	return cos(a)/sin(a);
}
