#include <stdio.h>
#include <string.h>

struct _Person
{
    char name[30];
    int heightcm;
    double weightkg;
};
// This abbreviates the type name
typedef struct _Person Person;
typedef Person* PPerson;
int main(int argc, char* argv[])
{
    Person Peter;
    PPerson pPeter;
    
    pPeter =&Peter;
    Peter.heightcm = 175;

    Peter.weightkg= 78.7;
    pPeter ->weightkg=83.2;
    printf("Peter's height: %d cm; Peter's weight:  %f kg \n", Peter.heightcm,Peter.weightkg); 

    return 0;
}
